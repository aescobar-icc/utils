package beauty.utils.serialize;

public class JSONException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public JSONException(String message, Throwable cause) {
		super(message, cause);
	}

	public JSONException(String message) {
		super(message);
	}
	
	
	

}
