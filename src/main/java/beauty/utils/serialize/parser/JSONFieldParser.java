package beauty.utils.serialize.parser;

public interface JSONFieldParser<T> {
	public T parse(JSONFieldInfo field);
}
