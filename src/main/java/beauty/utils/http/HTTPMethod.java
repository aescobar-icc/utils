package beauty.utils.http;

public enum HTTPMethod {
	HEAD,GET,POST,PUT,DELETE,TRACE,OPTIONS,CONNECT

}
