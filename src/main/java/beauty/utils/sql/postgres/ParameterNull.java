package beauty.utils.sql.postgres;

public class ParameterNull {
	private int type;

	public ParameterNull(int type) {
		super();
		this.type = type;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}
}
